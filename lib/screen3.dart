import 'dart:developer';

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';

class Screen3 extends StatefulWidget {
  const Screen3({
    Key? key,
    this.text = '',
  }) : super(key: key);
  final String? text;

  @override
  State<Screen3> createState() => _Screen3State();
}

class _Screen3State extends State<Screen3> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.deepOrange,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Screen3'),
              Text(((widget.text ?? '').isNotEmpty ? widget.text! : 'no data')),
            ],
          ),
        ),
      ),
    );
  }
}
