import 'dart:developer';

import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:test_link_project/screen3.dart';

class Screen2 extends StatefulWidget {
  const Screen2({Key? key, this.text = ''}) : super(key: key);
  final String? text;

  @override
  State<Screen2> createState() => _Screen2State();
}

class _Screen2State extends State<Screen2> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.red,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const Screen3()),
                    );
                  },
                  child: Text('Screen2')),
              Text(((widget.text ?? '').isNotEmpty ? widget.text! : 'no data')),
            ],
          ),
        ),
      ),
    );
  }
}
